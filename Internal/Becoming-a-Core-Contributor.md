This is a work-in-progress policy doc for the process of requesting and being granted F-Droid Core Contributor status.

* Maintainers of specific GitLab Projects like [_fdroiddata_](https://gitlab.com/fdroid/fdroiddata) or [RFP](https://gitlab.com/fdroid/rfp) can grant Reporter status as they see fit.  This is a good way to start out contributing to F-Droid to build a relationship in the core contributors.


*  All requests for Developer or higher status, or Reporter access to the full group should be made by submitting a request to the [_admin_](https://gitlab.com/fdroid/admin/issues) issue tracker.

* Two factor authentication (2FA) is encouraged for all F-Droid contributors.  All contributors with a group level of Developer, Maintainer, or Owner, or a project-level of Maintainer or Owner must use 2FA.  If an account in the @fdroid group does not have 2FA setup, then it will be set to "Reporter" status.

* Anyone who wishes to help or already has Developer access to any repo should get Reporter permissions on the whole @fdroid group
* Anyone who has contributed code to a repo and can be trusted can be granted Developer permissions on that repo
* The number of owners of the whole group should be kept at a minimum, but above 1-2 people to avoid a single point of failure

* it probably makes sense for RFP or _fdroiddata_ project Maintainers to add/remove people as Reporter/Developer to the project as needed. I don't think we need a whole procedure. 

* These requests are approved by "lazy consensus".  There is a one week waiting period where existing core contributors can voice concerns or objections.  If there have been no objections or requests for more time, the request will be approved after seven days has past.