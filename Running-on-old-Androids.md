F-Droid repos generally maintain support for old F-Droid releases for a long time.  That means that old F-Droid releases will continue to work for years.  That provides a path to run F-Droid on very old Android releases.  Here are the download links for installing the newest release that runs on a given Android release:

* Android 4.0 (`14`) to Android 5.0 (`21`): <br/> https://f-droid.org/archive/org.fdroid.fdroid_1012050.apk 
* Android 2.3.3 (`10`) to Android 3.x (`13`): <br/> https://f-droid.org/archive/org.fdroid.fdroid_1002052.apk 
* Android 2.2 (`8`) to Android 2.3 (`9`): <br/> https://f-droid.org/archive/org.fdroid.fdroid_100150.apk